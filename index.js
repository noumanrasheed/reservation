var express = require('express');
var mongoose = require('mongoose');
var cors = require('cors');
var path = require('path');
var bodyparser = require('body-parser');
var mongoose = require('mongoose');

var _router = require('./routes/contact');
var reservation_router = require('./routes/reservations');

const port = 3000;

var app = express();

mongoose.connect('mongodb://localhost:27017/Contacts', { useNewUrlParser: true });

mongoose.connection.on('connected', ()=>{
    console.log('Connected to DB');
});

mongoose.connection.on('error', ()=>{
    console.log('Error');
});

app.use(cors());

//body parser
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

//serving static content
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', _router);
app.use('/api', reservation_router);

app.listen(port,()=>{
    console.log(`The server is listening at port ${port}`);
});

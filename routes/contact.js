var express = require('express');
var router = express.Router();
const contactcontroller = require('../controller/contactController');

//login
router.post('/contact',contactcontroller.contact);
router.put('/updatecontact', contactcontroller.updatecontact);
router.get('/getallcontacts', contactcontroller.getallcontacts);
router.get('/getsinglecontact', contactcontroller.getsinglecontact);
router.delete('/deletecontact', contactcontroller.deletecontact);


module.exports= router
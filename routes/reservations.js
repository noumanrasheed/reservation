var express = require('express');
var router = express.Router();
const reservationController = require('../controller/reservationController');

//login
router.post('/insert_reservation',reservationController.insert_reservation);
router.put('/update_reservation', reservationController.update_reservation);
router.get('/get_all_reservations', reservationController.get_all_reservations);
router.get('/get_single_reservation', reservationController.get_single_reservation);
router.delete('/delete_reservation', reservationController.delete_reservation);


module.exports= router
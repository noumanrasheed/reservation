var mongoose = require('mongoose');

const contact_schema = mongoose.Schema({
    name : {
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true,
        index:{unique:true}
    },
    description:{
        type: String,
        require: false
    }
},{
    timestamp: true
});

module.exports = mongoose.model('contact_schema', contact_schema);
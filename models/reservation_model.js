var mongoose = require('mongoose');

const reservation_schema = mongoose.Schema({
    name : {
        type: String,
        require: true
    },
    email:{
        type: String,
        require: true,
        index:{unique:true}
    },
    room_type:{
        type: String,
        require: true,
        default: 'normal'
    },
    arrival_date:{
        type: Date,
        timestamp: true,
        default: Date.now
    },
    departure_date:{
        type: Date,
        timestamp: true,
        default: Date.now
    },
    no_of_guests:{
        type: Number,
        require: false,
        default: 1
    },
    free_pickup:{
        type: Boolean,
        require: true,
        default: false
    },
    flight_number:{
        type: String,
        require: false,
        default: ''
    },
    special_requests:{
        type: String,
        require: false,
        default: ''
    }
},{
    timestamp: true
});

module.exports = mongoose.model('reservation_schema', reservation_schema);
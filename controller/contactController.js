var nodemailer = require('nodemailer');
const contact_model = require('../models/contact_model');

const contact = (request, response)=>{

    //save contact logic
    _res = request.body;

    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
               user: 'iamnoumanrasheed@gmail.com',
               pass: 'n3tfl!x123'
           }
       });
    
    if(_res.name!=null && _res.email!=null && _res.description!=null){


        let newContact = new contact_model();
        newContact.name = _res.name;
        newContact.email = _res.email;
        newContact.description = _res.description;

        //Save Query
        newContact.save((error, contact)=>{

            if(error){
                response.send(error);
            }else{
                response.status(200).json({
                    'message': 'Data saved',
                    contact
                })
            }

        });

        //mail settings
        const mailOptions = {
            from: 'noumanrasheed@email.com', // sender address
            to: _res.email, // list of receivers
            subject: 'Contact Saved', // Subject line
            html: '<p>Your Contact has been saved Sucessfully</p>'// plain text body
          };

        transporter.sendMail(mailOptions, function (err, info) {
            if(err)
              console.log(err)
            else
              console.log(info);
         });
    }

}

const updatecontact = (request, response)=>{

    contact_model.findByIdAndUpdate(
        request.body.id,
        request.body.contact,
        {new:true},
        (error, contact)=>{
            if(error){
                response.send(error);
            }else{
                response.status(200).json({
                    "message": "Updated",
                    contact
                });
            }
        }
    )

}

const getallcontacts = (request, response)=>{
    contact_model.find((error,contacts)=>{
        if(error){
            response.send(error);
        }else{
            response.status(200).json({
                contacts
            });
        }
    });
}

const getsinglecontact = (request, response)=>{
    contact_model.findById(
        request.body.id
        , (error, contact)=>{
            if (error) {
                response.send(error)
            }else {
                response.status(200).json({
                    contact
                })
            }
        }
    )
}

const deletecontact = (request, response)=>{
    contact_model.remove(
        request.body.id
        , (error, contact)=>{
            if (error) {
                response.send(error)
            }else {
                response.status(200).json({
                    contact
                })
            }
        }
    )
}

module.exports = {
    contact,
    updatecontact,
    getallcontacts,
    getsinglecontact,
    deletecontact
}

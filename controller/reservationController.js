var nodemailer = require('nodemailer');
const reservation_model = require('../models/reservation_model');


const insert_reservation = (request, response) => {

    //save contact logic
    _res = request.body;

    if (
        _res.name != null
        && _res.email != null &&
        _res.room_type != null &&
        _res.arrival_date != null &&
        _res.departure_date != null &&
        _res.no_of_guests != null &&
        _res.free_pickup != null &&
        _res.flight_number != null &&
        _res.special_requests != null
    ) {

        var date = new Date();

        var current = new Date(); //'Mar 11 2015' current.getTime() = 1426060964567
        var followingDay = new Date(current.getTime() + 864000000);

        let newReservation = new reservation_model();
        newReservation.name = _res.name;
        newReservation.email = _res.email;
        newReservation.room_type = _res.room_type;
        newReservation.arrival_date = Date.now();
        newReservation.departure_date = followingDay.toLocaleDateString();
        newReservation.no_of_guests = _res.no_of_guests;
        newReservation.free_pickup = _res.free_pickup;
        newReservation.flight_number = _res.flight_number;
        newReservation.special_requests = _res.special_requests;

        //Save Query
        newReservation.save((error, reservation) => {

            if (error) {
                response.send(error);
            } else {
                response.status(200).json({
                    'message': 'Reservation saved Sucessfully',
                    reservation
                })
            }

        });


    }
}

const update_reservation = (request, response) => {

    reservation_model.findByIdAndUpdate(
        request.body.id,
        request.body.reservation,
        { new: true },
        (error, reservation) => {
            if (error) {
                response.send(error);
            } else {
                response.status(200).json({
                    "message": "Reservation Updated Sucessfully",
                    reservation
                });
            }
        }
    )

}

const get_all_reservations = (request, response)=>{
    reservation_model.find((error,reservation)=>{
        if(error){
            response.send(error);
        }else{
            response.status(200).json({
                reservation
            });
        }
    });
}

const get_single_reservation = (request, response)=>{
    reservation_model.findById(
        request.body.id
        , (error, reservation)=>{
            if (error) {
                response.send(error)
            }else {
                response.status(200).json({
                    reservation
                })
            }
        }
    )
}

const delete_reservation = (request, response)=>{
    reservation_model.remove(
        request.body.id
        , (error, reservation)=>{
            if (error) {
                response.send(error)
            }else {
                response.status(200).json({
                    reservation
                })
            }
        }
    )
}



module.exports = {
    insert_reservation,
    update_reservation,
    get_all_reservations,
    get_single_reservation,
    delete_reservation
}
